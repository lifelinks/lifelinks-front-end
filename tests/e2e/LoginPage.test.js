import { Selector } from 'testcafe';
// A fixture must be created for each group of tests.
fixture(`Login Page`)
    // Load the URL your development server runs on.
    .page('http://localhost:8080/login');

// const getUrl = ClientFunction(() => document.location.href.toString());

const btnSignin = Selector('#btnSignIn');
const btnRegister = Selector('#btnRegister');

// const btnSignInSubmit = Selector('#btnSignInSubmit');

const registerUsernameField = Selector('#registerUsername');
const registerEmail = Selector('#registerEmail');
const registerPasswordField = Selector('#registerPassword');
const registerPasswordConfirmField = Selector('#passwordConfirm');
const loginUsernameField = Selector('#username');
const loginPasswordField = Selector('#password');



test('Login Input Field Test', async t => {
    await t
        .click(btnSignin)
        .typeText(loginUsernameField, 'testusername')
        .typeText(loginPasswordField, 'TestPassword!1234')
        .expect(loginUsernameField.value).eql('testusername', 'Username Input should be testusername')
        .expect(loginPasswordField.value).eql('TestPassword!1234', 'Password Input should be TestPassword!1234')
});

test('Register Input Field Test', async t => {
    await t
        .click(btnRegister)
        .typeText(registerUsernameField, 'testusername')
        .typeText(registerEmail, 'email@test.nl')
        .typeText(registerPasswordField, 'TestPassword!1234')
        .typeText(registerPasswordConfirmField, 'TestPassword!1234')
        .expect(registerUsernameField.value).eql('testusername', 'Username Input should be testusername')
        .expect(registerEmail.value).eql('email@test.nl', 'Password Input should be TestPassword!1234')
        .expect(registerPasswordField.value).eql('TestPassword!1234', 'Password Input should be TestPassword!1234')
        .expect(registerPasswordConfirmField.value).eql('TestPassword!1234', 'Password Input should be TestPassword!1234')
})

// test('Loging in on test account', async t => {
//     await t
//         .click(btnSignin)
//         .typeText(loginUsernameField, 'testusername1234')
//         .typeText(loginPasswordField, 'TestPass!1234')
//         .click(btnSignInSubmit)
//         .expect(getUrl()).contains('/explore')
// })