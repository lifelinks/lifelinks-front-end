// import { Selector } from 'testcafe';
// // A fixture must be created for each group of tests.
// fixture(`Explore Page`)
//     // Load the URL your development server runs on.
//     .page('http://localhost:8080/login');

// const btnSignInSubmit = Selector('#btnSignInSubmit');

// const loginUsernameField = Selector('#username');
// const loginPasswordField = Selector('#password');

// const btnMessagePostCreate = Selector('#btnMessagePostCreate')
// const newPostTextArea = Selector('#newPostTextArea')

// var smallLorem = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras dolor mauris, volutpat sit amet massa at, gravida congue mi."
// var bigLorem = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent elementum eget risus sit amet ornare. Mauris viverra, sem non tincidunt vulputate, purus lacus fringilla leo, ac faucibus ante nibh eu arcu. Nullam pulvinar vel est ut dignissim. Integer in semper orci. Pellentesque malesuada tortor lectus. Fusce tortor neque, pharetra ac rhoncus nec, pellentesque sit amet elit. Nullam aliquam imperdiet est nec varius. Proin hendrerit pulvinar elit, et tincidunt neque semper eu. Vestibulum efficitur dictum nisi, sed rutrum purus. Mauris sed sodales risus. Nullam id purus quis risus fermentum placerat. Sed finibus porta pretium. Fusce congue nulla et erat elementum rutrum. Curabitur augue erat, pharetra vel efficitur vel, lobortis nec nunc. Pellentesque condimentum ligula in diam imperdiet venenatis. Pellentesque ac mi eu sem sodales accumsan nec maximus sapien. Nulla nec lorem nec nisi molestie commodo. Maecenas imperdiet cursus quam in facilisis."


// test('Test making post on test account', async t => {
//     await t
//         .typeText(loginUsernameField, 'testusername1234')
//         .typeText(loginPasswordField, 'TestPass!1234')
//         .click(btnSignInSubmit)
//         .wait(5000)
//         .click(btnMessagePostCreate)
//         .typeText(newPostTextArea, smallLorem)
//         .expect(newPostTextArea.value).eql(smallLorem, 'newPostTextArea should be equel to smallLorem value')
// });

// test('Test making post with too many characters', async t => {
//     await t
//     .typeText(loginUsernameField, 'testusername1234')
//     .typeText(loginPasswordField, 'TestPass!1234')
//     .click(btnSignInSubmit)
//     .wait(5000)
//     .click(btnMessagePostCreate)
//     .typeText(newPostTextArea, bigLorem)
//     .expect(newPostTextArea.value).match(/^.{160,160}$/, 'newPostTextArea should only be able to hold 160 characters')
// })