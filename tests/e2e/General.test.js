import { Selector } from 'testcafe';
// A fixture must be created for each group of tests.
fixture(`Config Test`)
    // Load the URL your development server runs on.
    .page('http://localhost:8080');

const btnThemeSwitch = Selector('#btnThemeSwitch');


test('Set Darkmode On', async t => {
    await t
        .click(btnThemeSwitch)
});