import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [{
        path: '/',
        name: 'Login',
        component: () =>
            import ("../views/Login.vue"),
        meta: {
            requiresAuth: false,
        },

    },
    {
        path: '/login',
        name: 'Login',
        component: () =>
            import ('../views/Login.vue'),
        meta: {
            requiresAuth: false,
        },
    },
    {
        path: '/explore',
        name: 'explore',
        component: () =>
            import ("../views/Explore.vue"),
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: '/projects',
        name: 'projects',
        component: () =>
            import ('../views/Projects.vue'),
        meta: {
            requiresAuth: false,
        },
    },
    {
        path: '/team',
        name: 'team',
        component: () =>
            import ('../views/Team.vue'),
        meta: {
            requiresAuth: false,
        },
    },
    {
        path: '/placeholder',
        name: 'placeholder',
        component: () =>
            import ('../views/Placeholder.vue'),
        meta: {
            requiresAuth: false,
        },
    },
    {
        path: '*',
        name: '404',
        component: () =>
            import ('../views/404.vue'),
        meta: {
            requiresAuth: false,
        },
    },
    {
        path: '/dashboard',
        name: 'dashboard',
        component: () =>
            import ('../views/Home.vue'),
        meta: {
            requiresAuth: false,
        },
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
});

router.beforeEach((to, from, next) => {
    if (to.matched.some((record) => record.meta.requiresAuth)) {
        if (localStorage.jwt_token == null) {
            next({
                path: '/login',
                params: { nextUrl: to.fullPath },
            });
        } else next();
    } else if (to.matched.some((record) => record.name == 'Login')) {
        if (localStorage.jwt_token != null) {
            next({
                path: '/explore'
            });
        } else next();
    } else {
        next();
    }
})

export default router