import Vue from 'vue';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        options: {
            customProperties: true,
        },
        themes: {
            light: {
                primary: '#27b83e',
                secondary: '#FF8C42',
                accent: '#27b83e',
                error: '#FF5252',
                info: '#2196F3',
                success: '#4CAF50',
                warning: '#FFC107',
                background: '#F7F7FF'
            },
            dark: {
                primary: '#27b83e',
                secondary: '#FF8C42',
                accent: '#27b83e',
                error: '#FF5252',
                info: '#2196F3',
                success: '#4CAF50',
                warning: '#FFC107',
            }
        },
        dark: localStorage.getItem("darkTheme") == "true" ? true : false,
    },
    icons: {
        iconfont: 'mdi'
    }
});